package com.br.mastertech.mscallcep.controller

import com.br.mastertech.mscallcep.models.RqCep
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable


@FeignClient("checkcompanycontroller", url = "http://viacep.com.br/ws/")
interface GetCEPController {
    @GetMapping("{cep}/json/", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun get(@PathVariable("cep") cep: String) : RqCep
}