package com.br.mastertech.mscallcep.controller

import com.br.mastertech.mscallcep.models.RqCep
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class RequestCep {

    @Autowired
    lateinit var controller: GetCEPController

    @GetMapping("/cep/{cep}")
    fun get(@PathVariable("cep") cep: String) : RqCep {
        return controller.get(cep.replace(Regex("\\D"), ""))
    }
}