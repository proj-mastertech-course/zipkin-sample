package br.com.mastertech.sample.zipkincep.controller

import br.com.mastertech.sample.zipkincep.models.Mapper
import br.com.mastertech.sample.zipkincep.models.RqUser
import br.com.mastertech.sample.zipkincep.models.RsUser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*


@RestController
class CreateUserController {

    @Autowired
    lateinit var requestCEPController: RequestCEPController

    @PostMapping("/create/user")
    fun create(@RequestBody rqUser: RqUser) : RsUser {
        println(rqUser)
        val cep = rqUser.cep.replace(Regex("\\D")
                , "")
        println(cep)
        val rqCep = requestCEPController.requestCep(cep)
        println("Request CEP: $rqCep")
        return Mapper.fromRqCepToRsUser(rqUser.name, rqCep)
    }
}