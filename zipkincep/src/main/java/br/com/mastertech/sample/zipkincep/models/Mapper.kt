package br.com.mastertech.sample.zipkincep.models

object Mapper {

    @JvmStatic
    fun fromRqCepToRsUser(name: String, rqCep: RqCep) : RsUser =
            RsUser(name, RsCep(rqCep.cep, rqCep.bairro, rqCep.complemento))
}