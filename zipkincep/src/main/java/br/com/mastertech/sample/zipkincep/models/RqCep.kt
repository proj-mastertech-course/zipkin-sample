package br.com.mastertech.sample.zipkincep.models

data class RqCep(
        var cep: String? = null,
        var logradouro: String? = null,
        var complemento: String? = null,
        var bairro: String? = null,
        var localidade: String? = null,
        var uf: String? = null,
        var unidade: String? = null,
        var ibge: String? = null,
        var gia: String? = null)