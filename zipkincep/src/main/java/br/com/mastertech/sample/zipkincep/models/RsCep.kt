package br.com.mastertech.sample.zipkincep.models

data class RsCep(val cep: String?, val bairro: String?, val complemento: String?)