package br.com.mastertech.sample.zipkincep.models

import com.fasterxml.jackson.annotation.JsonProperty

class RqUser(@JsonProperty("name") val name: String
             , @JsonProperty("cep") val cep: String)