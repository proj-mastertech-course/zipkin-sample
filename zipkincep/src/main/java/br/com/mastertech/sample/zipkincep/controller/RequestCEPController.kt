package br.com.mastertech.sample.zipkincep.controller

import br.com.mastertech.sample.zipkincep.models.RqCep
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable


@FeignClient("mscallcep")
interface RequestCEPController {
    @GetMapping("/cep/{cep}")
    fun requestCep(@PathVariable("cep") cep: String) : RqCep
}